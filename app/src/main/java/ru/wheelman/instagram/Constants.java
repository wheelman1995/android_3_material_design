package ru.wheelman.instagram;

public final class Constants {
    public static final String MAIN_SHARED_PREFERENCES_NAME = "main";
    public static final String MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY = "black_theme_enabled";

    private Constants() {
    }
}
