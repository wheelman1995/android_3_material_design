package ru.wheelman.instagram;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    private boolean blackThemeEnabled;
    private DrawerLayout drawerLayout;

    private Toolbar toolbar;
    private ViewStubCompat viewStub;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme();

        super.setContentView(R.layout.activity_base);

        initUI();

        initListeners();
    }

    private void initListeners() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                drawerLayout.closeDrawer(GravityCompat.START, true);

                switch (item.getItemId()) {
                    case R.id.activity_base_navigation_view_menu_item_settings:
                        goToSettingsActivity();
                        return true;
                    case R.id.activity_base_navigation_view_menu_item_cards:
                        goToCardsActivity();
                        return true;
                    default:
                        return false;
                }

            }
        });
    }

    private void goToCardsActivity() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    @Override
    @CallSuper
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START, true);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void goToSettingsActivity() {
        Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    private void setTheme() {
        blackThemeEnabled = Prefs.getINSTANCE().isBlackThemeEnabled();
        setTheme(blackThemeEnabled ? R.style.BlackAppTheme : R.style.LightAppTheme);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setContentView(int layoutResID) {
        viewStub.setLayoutResource(layoutResID);
        viewStub.inflate();
    }

    private void initUI() {
        toolbar = findViewById(R.id.t_activity_base);
        setSupportActionBar(toolbar);

        viewStub = findViewById(R.id.vsc_activity_base);
        drawerLayout = findViewById(R.id.dl_activity_base);
        navigationView = findViewById(R.id.nv_activity_base);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START, true);
            return;
        }
        super.onBackPressed();
    }

}
