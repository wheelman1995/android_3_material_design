package ru.wheelman.instagram;

import java.util.Observable;

public class ObservableLong extends Observable {
    private Long l;

    public void set(Long newValue) {
        this.l = newValue;
        setChanged();
        notifyObservers(l);
    }
}
