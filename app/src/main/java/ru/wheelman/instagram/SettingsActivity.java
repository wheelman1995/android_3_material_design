package ru.wheelman.instagram;


import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.switchmaterial.SwitchMaterial;

import androidx.core.app.AlarmManagerCompat;

import static ru.wheelman.instagram.Constants.MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY;
import static ru.wheelman.instagram.Constants.MAIN_SHARED_PREFERENCES_NAME;

public class SettingsActivity extends BaseActivity {

    private SwitchMaterial blackThemeSwitch;
    private boolean blackThemeEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        View v = getLayoutInflater().inflate(R.layout.activity_settings, getRootViewGroup(), false);
        setContentView(R.layout.activity_settings);

        initVariables();

        initListeners();
    }

    @SuppressLint("ApplySharedPref")
    private void initListeners() {
        blackThemeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Prefs.getINSTANCE().setBlackThemeEnabled(isChecked);
            restartApp();
        });
    }

    private void restartApp() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);

        AlarmManagerCompat.setExact(alarmManager, AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), pendingIntent);
    }

    private void initVariables() {
        blackThemeEnabled = Prefs.getINSTANCE().isBlackThemeEnabled();
        blackThemeSwitch = findViewById(R.id.sm_black_theme);
        blackThemeSwitch.setChecked(blackThemeEnabled);
    }
}
