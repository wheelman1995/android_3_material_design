package ru.wheelman.instagram;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.room.PhotoFile;

public class PhotoFilesViewModel extends AndroidViewModel {
    public PhotoFilesViewModel(@NonNull Application application) {
        super(application);
        repository = PhotoFilesRepositoryImpl.getRepository(application);
        allPhotoFiles = repository.getAllPhotoFiles();
    }
    private PhotoFilesRepository repository;
    private LiveData<List<PhotoFile>> allPhotoFiles;

    public LiveData<List<PhotoFile>> getAllPhotoFiles() {
        return allPhotoFiles;
    }

    public ObservableBoolean insert(PhotoFile photoFile) {
        return repository.insert(photoFile);
    }

    public ObservableBoolean delete(PhotoFile photoFile) {
        return repository.delete(photoFile);
    }

    public ObservableBoolean update(PhotoFile photoFile) {
        return repository.update(photoFile);
    }
}
