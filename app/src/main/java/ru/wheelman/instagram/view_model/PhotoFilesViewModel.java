package ru.wheelman.instagram.view_model;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.ObservableLong;
import ru.wheelman.instagram.ObservablePhotoFilesList;
import ru.wheelman.instagram.repository.IPhotoFilesRepository;
import ru.wheelman.instagram.repository.PhotoFilesRepository;
import ru.wheelman.instagram.room.PhotoFile;

public class PhotoFilesViewModel extends AndroidViewModel {
    public PhotoFilesViewModel(@NonNull Application application) {
        super(application);
        repository = PhotoFilesRepository.getRepository(application);
        allPhotoFiles = repository.getAllPhotoFiles();
    }
    private IPhotoFilesRepository repository;
    private LiveData<List<PhotoFile>> allPhotoFiles;

    public LiveData<List<PhotoFile>> getAllPhotoFiles() {
        return allPhotoFiles;
    }

    public ObservableLong insert(PhotoFile photoFile) {
        return repository.insert(photoFile);
    }

    public ObservableBoolean delete(PhotoFile photoFile) {
        return repository.delete(photoFile);
    }

    public ObservableBoolean update(PhotoFile photoFile) {
        return repository.update(photoFile);
    }

    public ObservablePhotoFilesList getAllLikedPhotoFiles() {
        return repository.getAllLikedPhotoFiles();
    }
}
