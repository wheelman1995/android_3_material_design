package ru.wheelman.instagram;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import ru.wheelman.instagram.room.PhotoFile;

public class PhotoViewerFragment extends Fragment {

    public static final String PHOTO_FILE_BUNDLE_KEY = "photo";
    public static final String TRANSITION_NAME_BUNDLE_KEY = "transition_name";
    private PhotoFile photoFile;
    private AppCompatImageView photoView;


    public static PhotoViewerFragment newInstance(Bundle bundle) {
        PhotoViewerFragment photoViewerFragment = new PhotoViewerFragment();
        photoViewerFragment.setArguments(bundle);
        return photoViewerFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo_viewer, container, false);

        initVariables(v);

        return v;
    }

    private void initVariables(View v) {
        photoFile = (PhotoFile) getArguments().getSerializable(PHOTO_FILE_BUNDLE_KEY);
        String transitionName = getArguments().getString(TRANSITION_NAME_BUNDLE_KEY);

        photoView = v.findViewById(R.id.aciv_fragment_photo_viewer);
        photoView.setImageBitmap(BitmapFactory.decodeFile(photoFile.getPath()));
        photoView.setTransitionName(transitionName);
    }
}
