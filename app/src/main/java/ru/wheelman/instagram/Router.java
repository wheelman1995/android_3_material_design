package ru.wheelman.instagram;

import android.content.Context;
import android.content.Intent;

import ru.wheelman.instagram.view.App;

public class Router {

    private Router() {
    }

    public static Router getInstance() {
        return RouterHolder.INSTANCE;
    }

    //can not call this method here
    // android.util.AndroidRuntimeException: Calling startActivity() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag.
    public void startNextActivity(Context activityContext, Class activity) {
        Intent intent = new Intent(activityContext, activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        App.getInstance().startActivity(intent);
    }

    private static class RouterHolder {
        private static final Router INSTANCE = new Router();
    }
}
