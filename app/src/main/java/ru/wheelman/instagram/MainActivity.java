package ru.wheelman.instagram;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import androidx.transition.TransitionInflater;
import androidx.transition.TransitionSet;
import ru.wheelman.instagram.room.PhotoFile;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final long CARD_TRANSITION_DURATION = 300;
    private CardsFragment cardsFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initVariables();

        initListener();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.cfl_activity_main, cardsFragment = CardsFragment.newInstance())
                .commit();

    }

    private void initListener() {

    }

    private void initVariables() {


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        switch (id) {
//            case R.id.activity_main_options_menu_item_settings:
//                return true;
//        }
//
//
//        return super.onOptionsItemSelected(item);
//    }

    public void onPhotoViewClick(View v, PhotoFile photoFile) {

        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move));
        transitionSet.setDuration(CARD_TRANSITION_DURATION);

        Bundle bundle = new Bundle();
        bundle.putSerializable(PhotoViewerFragment.PHOTO_FILE_BUNDLE_KEY, photoFile);
        bundle.putString(PhotoViewerFragment.TRANSITION_NAME_BUNDLE_KEY, v.getTransitionName());

        PhotoViewerFragment photoViewerFragment = PhotoViewerFragment.newInstance(bundle);
        photoViewerFragment.setSharedElementEnterTransition(transitionSet);
        photoViewerFragment.setSharedElementReturnTransition(transitionSet);

        cardsFragment.setExitTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.explode));

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.cfl_activity_main, photoViewerFragment)
                .addToBackStack(null)
                .addSharedElement(v, v.getTransitionName())
                .commit();
    }

}
