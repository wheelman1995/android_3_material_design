package ru.wheelman.instagram;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import ru.wheelman.instagram.room.PhotoFile;

public class CardsFragmentRecyclerViewAdapter extends RecyclerView.Adapter<CardsFragmentRecyclerViewAdapter.ViewHolder> {

    private HeartViewOnClickListener heartViewOnClickListener;
    private CardViewOnLongClickListener cardViewOnLongClickListener;
    private OnPhotoViewClickListener onPhotoViewClickListener;
    private List<PhotoFile> photoFiles;
    private Context context;
    private boolean animateHeart;
    private ViewHolder currentHolder;
    private int currentPosition;


    public CardsFragmentRecyclerViewAdapter(List<PhotoFile> photoFiles) {
        this.photoFiles = photoFiles;

    }

    public void setData(List<PhotoFile> photoFiles) {

        this.photoFiles = photoFiles;
        notifyDataSetChanged();

    }

    public void removeItem(int position) {
        photoFiles.remove(position);
        notifyItemRemoved(position);
    }

    public void setHeartViewOnClickListener(HeartViewOnClickListener heartViewOnClickListener) {
        this.heartViewOnClickListener = heartViewOnClickListener;
    }

    public void setCardViewOnLongClickListener(CardViewOnLongClickListener cardViewOnLongClickListener) {
        this.cardViewOnLongClickListener = cardViewOnLongClickListener;
    }

    public void setOnPhotoViewClickListener(OnPhotoViewClickListener onPhotoViewClickListener) {
        this.onPhotoViewClickListener = onPhotoViewClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        CardView cardView = (CardView) LayoutInflater.from(context).inflate(R.layout.fragment_cards_recycler_view_adapter_item, parent, false);

        return new ViewHolder(cardView);
    }

//    @Override
//    public void onBindViewHolder(@NonNull ViewHolder holder, int position, @NonNull List<Object> payloads) {
//        if (payloads.isEmpty()) {
//            super.onBindViewHolder(holder, position, payloads);
//            return;
//        }
//
//    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        currentHolder = holder;
        currentPosition = position;

        initCurrentHolder();

        initCurrentHolderListeners();

    }


    private void initCurrentHolder() {
        PhotoFile photoFile = photoFiles.get(currentPosition);

        currentHolder.photoFile = photoFile;

        Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getPath());
        currentHolder.photoView.setImageBitmap(bitmap);
        currentHolder.photoView.setTransitionName(context.getString(R.string.card_transition_name) + currentPosition);

        if (photoFile.isLiked()) {
            if (animateHeart) {
                AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) context.getDrawable(R.drawable.avd_outlined_to_filled_heart);
                currentHolder.heartView.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                animateHeart = false;
            } else {
                currentHolder.heartView.setImageDrawable(context.getDrawable(R.drawable.filled_heart));
            }
        } else {
            currentHolder.heartView.setImageDrawable(context.getDrawable(R.drawable.outlined_heart));
        }


    }

    private void initCurrentHolderListeners() {
        ViewHolder holder = currentHolder;
        PhotoFile photoFile = holder.photoFile;


        holder.photoView.setOnClickListener(v -> {
            if (onPhotoViewClickListener != null) {
                onPhotoViewClickListener.onClick(v, photoFile, holder.getLayoutPosition());
            }
        });

        holder.photoView.setOnLongClickListener(v -> {
            holder.cardView.performLongClick();
            return true;
        });

        holder.cardView.setOnLongClickListener(v -> {
            if (cardViewOnLongClickListener != null) {
                cardViewOnLongClickListener.onLongClick(v, photoFile, holder.getLayoutPosition());
            }
            return true;
        });

        holder.heartView.setOnClickListener(v -> {
            if (heartViewOnClickListener != null) {
                heartViewOnClickListener.onClick(v, photoFile, holder.getLayoutPosition());
                animateHeart = true;
            }
        });
    }

    private Bitmap createBitmap(AppCompatImageView imageView, File photoFile) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeFile(photoFile.getAbsolutePath(), bmOptions);
    }

    @Override
    public int getItemCount() {
        return photoFiles.size();
    }

    public void changeItem(PhotoFile newPhotoFile, int position) {
        photoFiles.remove(position);
        photoFiles.add(position, newPhotoFile);
        notifyItemChanged(position);
    }

    public void insertItem(PhotoFile photoFile) {
        photoFiles.add(0, photoFile);
        notifyItemInserted(0);
    }

    public interface OnPhotoViewClickListener {
        void onClick(View v, PhotoFile photoFile, int position);
    }

    public interface CardViewOnLongClickListener {
        void onLongClick(View view, PhotoFile photoFile, int position);
    }

    public interface HeartViewOnClickListener {
        void onClick(View view, PhotoFile photoFile, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private PhotoFile photoFile;
        private CardView cardView;
        private AppCompatImageView photoView;
        private AppCompatImageView heartView;

        public ViewHolder(@NonNull CardView cardView) {
            super(cardView);
            this.cardView = cardView;
            photoView = cardView.findViewById(R.id.aciv_fragment_cards_recycler_view_adapter_item_photo);
            heartView = cardView.findViewById(R.id.aciv_fragment_cards_recycler_view_adapter_item_heart);
        }
    }
}
