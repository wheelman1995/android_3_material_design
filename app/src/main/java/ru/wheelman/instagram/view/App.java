package ru.wheelman.instagram.view;

import android.app.Application;

public class App extends Application {

    private static App app;

    public static App getInstance() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
    }

    public void restartApp() {
//        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
//
//        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        AlarmManagerCompat.setExact(alarmManager, AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 100, pendingIntent);

        System.exit(0);
    }
}
