package ru.wheelman.instagram.view.fragments;

import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.util.Collections;
import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.CallSuper;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.R;
import ru.wheelman.instagram.room.PhotoFile;
import ru.wheelman.instagram.view.activities.MainActivity;
import ru.wheelman.instagram.view.fragments.cards_fragment.CardsFragmentRecyclerViewAdapter;
import ru.wheelman.instagram.view_model.PhotoFilesViewModel;

public abstract class BaseCardsFragment extends Fragment {

    private static final int RECYCLER_VIEW_COLUMN_NUMBER = 4;
    protected RecyclerView rv;
    protected CardsFragmentRecyclerViewAdapter adapter;
    protected PhotoFilesViewModel photoFilesViewModel;
    private ActionMode.Callback actionModeCallback;
    private ActionMode actionMode;
    private PhotoFile photoFileToDelete;
    private int positionOfFileBeingDeleted;
    private View viewBeingDeleted;
    private Drawable backgroundBackup;
    private ViewChangedListener viewChangedListener;

    public void setViewChangedListener(ViewChangedListener viewChangedListener) {
        this.viewChangedListener = viewChangedListener;
    }

    protected void sendOnViewChangedCallback() {
        if (viewChangedListener != null) {
            viewChangedListener.onViewChanged();
        }
    }

    @CallSuper
    protected void initListeners() {
        adapter.setOnPhotoViewClickListener((v, photoFile, position) -> {
            ((MainActivity) getActivity()).onPhotoViewClick(v, photoFile);
        });

        adapter.setCardViewOnLongClickListener((v, photoFile, position) -> {

            if (actionMode != null) {
                viewBeingDeleted.setBackground(backgroundBackup);
            }

            viewBeingDeleted = v;
            backgroundBackup = viewBeingDeleted.getBackground();
            photoFileToDelete = photoFile;
            positionOfFileBeingDeleted = position;
            actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(actionModeCallback);
            viewBeingDeleted.setBackgroundResource(R.drawable.rectangle);

        });

        actionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.fragment_cards_action_menu, menu);

                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.fragment_cards_action_menu_item_delete:
                        deleteSelectedPhoto();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;
                viewBeingDeleted.setBackground(backgroundBackup);
            }
        };

//        adapter.setHeartViewOnClickListener((v, photoFile, position) -> {
//            photoFile.setLiked(!photoFile.isLiked());
//            ObservableBoolean observableBoolean = photoFilesViewModel.update(photoFile);
//
//            observableBoolean.addObserver((o, newValue) -> {
//                boolean success = (boolean) newValue;
//
//                if (success) {
//                    getActivity().runOnUiThread(() -> {
//                        adapter.changeItem(photoFile, position, photoFile.isLiked());
//                        sendOnViewChangedCallback();
//                    });
//                }
//                observableBoolean.deleteObservers();
//
//            });
//
//        });
    }

    private void deleteSelectedPhoto() {

        Timer timer = new Timer();

        TimerTask timerTask = new TimerTask() {

            PhotoFile photoFileToDelete = BaseCardsFragment.this.photoFileToDelete;
            int positionOfFileBeingDeleted = BaseCardsFragment.this.positionOfFileBeingDeleted;

            @Override
            public void run() {

                ObservableBoolean observableBoolean = photoFilesViewModel.delete(photoFileToDelete);
                observableBoolean.addObserver((o, newValue) -> {
                    boolean success = (boolean) newValue;

                    if (success) {
                        new File(photoFileToDelete.getPath()).delete();

                        getActivity().runOnUiThread(() -> {
                            adapter.removeItem(positionOfFileBeingDeleted);
                            sendOnViewChangedCallback();
                        });

                        observableBoolean.deleteObservers();
                    }
                });
            }
        };

        Snackbar.make(rv, R.string.cards_fragment_snackbar_photo_deletion_warning, Snackbar.LENGTH_LONG)
                .setAction(R.string.cards_fragment_snackbar_photo_deletion_action_undo, v -> {
                    timer.cancel();
                }).show();

        timer.schedule(timerTask, 3000);
    }

    @CallSuper
    protected void initUi(View v) {
        photoFilesViewModel = ViewModelProviders.of(this).get(PhotoFilesViewModel.class);

        rv = v.findViewById(R.id.recycler_view);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new GridLayoutManager(v.getContext(), RECYCLER_VIEW_COLUMN_NUMBER));

        adapter = new CardsFragmentRecyclerViewAdapter(Collections.emptyList());
        rv.setAdapter(adapter);
    }


    public interface ViewChangedListener {
        void onViewChanged();
    }

}
