package ru.wheelman.instagram.view.activities;


import android.annotation.SuppressLint;
import android.os.Bundle;

import com.google.android.material.switchmaterial.SwitchMaterial;

import ru.wheelman.instagram.view.App;
import ru.wheelman.instagram.Prefs;
import ru.wheelman.instagram.R;

public class SettingsActivity extends BaseActivity {

    private SwitchMaterial blackThemeSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        View v = getLayoutInflater().inflate(R.layout.activity_settings, getRootViewGroup(), false);
        setContentView(R.layout.activity_settings);

        initUi();

        initListeners();
    }

    @SuppressLint("ApplySharedPref")
    private void initListeners() {
        blackThemeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Prefs.getInstance().setBlackThemeEnabled(isChecked);
            App.getInstance().restartApp();
        });
    }

    private void initUi() {
        blackThemeSwitch = findViewById(R.id.sm_black_theme);
        blackThemeSwitch.setChecked(Prefs.getInstance().isBlackThemeEnabled());
    }
}
