package ru.wheelman.instagram.view.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.appcompat.widget.ViewStubCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import ru.wheelman.instagram.Prefs;
import ru.wheelman.instagram.R;

public abstract class BaseActivity extends AppCompatActivity {

    private static final String TAG = BaseActivity.class.getSimpleName();
    private DrawerLayout drawerLayout;

    private Toolbar toolbar;
    private ViewStubCompat viewStub;
    private NavigationView navigationView;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private boolean navigationDrawerIconIsBackArrow;
    private AnimatedVectorDrawable hamburgerToBackArrow;
    private AnimatedVectorDrawable backArrowToHamburger;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme();

        super.setContentView(R.layout.activity_base);

        initUI();

        initListeners();
    }

    private void initListeners() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                drawerLayout.closeDrawer(GravityCompat.START, true);

                switch (item.getItemId()) {
                    case R.id.activity_base_navigation_view_menu_item_settings:
                        goToSettingsActivity();
                        return true;
                    case R.id.activity_base_navigation_view_menu_item_cards:
                        goToCardsActivity();
                        return true;
                    default:
                        return false;
                }

            }
        });

        actionBarDrawerToggle.setToolbarNavigationClickListener(v -> {
            onBackPressed();
        });
    }

    public void toggleNavigationDrawerIcon() {
        navigationDrawerIconIsBackArrow = !navigationDrawerIconIsBackArrow;

        actionBarDrawerToggle.setHomeAsUpIndicator(navigationDrawerIconIsBackArrow ?
                hamburgerToBackArrow : backArrowToHamburger);

        if (navigationDrawerIconIsBackArrow) {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
            hamburgerToBackArrow.start();
        } else {
            backArrowToHamburger.start();

            enableDrawerIndicatorWhenBackArrowToHamburgerAnimationIsFinished();

        }

    }

    private void enableDrawerIndicatorWhenBackArrowToHamburgerAnimationIsFinished() {

        Timer timer = new Timer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
            }
        };

        timer.schedule(timerTask, getResources().getInteger(R.integer.navigation_drawer_icon_animation_duration));
    }

    private void goToCardsActivity() {
        startNextActivity(MainActivity.class);
    }

//    @Override
//    @CallSuper
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                Log.d(TAG, "onOptionsItemSelected home");
//                if (navigationDrawerIconIsBackArrow) {
//                    onBackPressed();
//                } else {
//                    drawerLayout.openDrawer(GravityCompat.START, true);
//                }
//                return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    public void startNextActivity(Class activity) {
        Intent intent = new Intent(getApplicationContext(), activity);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    private void goToSettingsActivity() {
        startNextActivity(SettingsActivity.class);
    }

    private void setTheme() {
        setTheme(Prefs.getInstance().isBlackThemeEnabled() ? R.style.BlackAppTheme : R.style.LightAppTheme);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setContentView(int layoutResID) {
        viewStub.setLayoutResource(layoutResID);
        viewStub.inflate();
    }

    private void initUI() {
        toolbar = findViewById(R.id.t_activity_base);
        setSupportActionBar(toolbar);

        viewStub = findViewById(R.id.vsc_activity_base);
        drawerLayout = findViewById(R.id.dl_activity_base);
        navigationView = findViewById(R.id.nv_activity_base);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, 0, 0);

        hamburgerToBackArrow = (AnimatedVectorDrawable) getDrawable(R.drawable.avd_hamburger_to_back_arrow);
        backArrowToHamburger = (AnimatedVectorDrawable) getDrawable(R.drawable.avd_back_arrow_to_hamburger);

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START, true);
            return;
        }
        super.onBackPressed();
    }

}
