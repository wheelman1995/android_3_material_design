package ru.wheelman.instagram.view;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import ru.wheelman.instagram.R;
import ru.wheelman.instagram.view.fragments.FavoritesFragment;
import ru.wheelman.instagram.view.fragments.cards_fragment.CardsFragment;

public class PagerAdapter extends FragmentPagerAdapter {
    private static final int FRAGMENT_COUNT = 2;
    private CardsFragment cardsFragment;
    private FavoritesFragment favoritesFragment;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (cardsFragment == null) {
                    cardsFragment = CardsFragment.newInstance();
                    cardsFragment.setViewChangedListener(() -> favoritesFragment.updateData());
                }
                return cardsFragment;
            case 1:
                if (favoritesFragment == null) {
                    favoritesFragment = FavoritesFragment.newInstance();
                    favoritesFragment.setViewChangedListener(() -> cardsFragment.updateData());
                }
                return favoritesFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return App.getInstance().getString(R.string.cards_fragment_tab_title);
            case 1:
                return App.getInstance().getString(R.string.favorites_fragment_tab_title);
            default:
                return null;
        }
    }


}
