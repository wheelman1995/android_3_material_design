package ru.wheelman.instagram.view.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.ObservablePhotoFilesList;
import ru.wheelman.instagram.R;
import ru.wheelman.instagram.room.PhotoFile;

public class FavoritesFragment extends BaseCardsFragment {

    private static final String TAG = FavoritesFragment.class.getSimpleName();

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        initUi(rootView);

        initListeners();

        updateData();

        return rootView;
    }

    @Override
    protected void initUi(View v) {
        super.initUi(v);
    }

    public void updateData() {
        ObservablePhotoFilesList observablePhotoFilesList = photoFilesViewModel.getAllLikedPhotoFiles();
        observablePhotoFilesList.addObserver((o, list) -> {
            List<PhotoFile> l = (List<PhotoFile>) list;

            if (l != null) {
                getActivity().runOnUiThread(() -> {
                    adapter.setData(l);
                });
            }
            observablePhotoFilesList.deleteObservers();
        });
    }

    @Override
    protected void initListeners() {
        super.initListeners();

        adapter.setHeartViewOnClickListener((v, photoFile, position) -> {
            Log.d(TAG, "setHeartViewOnClickListener position " + position);
            photoFile.setLiked(!photoFile.isLiked());
            ObservableBoolean observableBoolean = photoFilesViewModel.update(photoFile);

            observableBoolean.addObserver((o, newValue) -> {
                boolean success = (boolean) newValue;

                if (success) {
                    getActivity().runOnUiThread(() -> {
                        adapter.removeItem(position);
                        sendOnViewChangedCallback();
                    });
                }
                observableBoolean.deleteObservers();

            });

        });
    }
}
