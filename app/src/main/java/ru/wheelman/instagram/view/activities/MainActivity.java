package ru.wheelman.instagram.view.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.transition.TransitionInflater;
import androidx.transition.TransitionSet;
import ru.wheelman.instagram.R;
import ru.wheelman.instagram.room.PhotoFile;
import ru.wheelman.instagram.view.fragments.CommonFragment;
import ru.wheelman.instagram.view.fragments.PhotoViewerFragment;

public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final long CARD_TRANSITION_DURATION = 300;
    private BottomNavigationView bottomNavigationView;
    private CommonFragment commonFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initUi();

        initListener();

    }

    private void initListener() {
        bottomNavigationView.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.activity_main_bottom_navigation_view_menu_item_common:
                    if (commonFragment == null) {
                        commonFragment = CommonFragment.newInstance();
                    }
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, commonFragment)
                            .commit();
                    return true;
                case R.id.activity_main_bottom_navigation_view_menu_item_network:

                    return true;
                case R.id.activity_main_bottom_navigation_view_menu_item_database:

                    return true;
                default:
                    return false;
            }
        });
    }

    private void initUi() {
        bottomNavigationView = findViewById(R.id.bnv_activity_main);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        int id = item.getItemId();
//
//        switch (id) {
//            case R.id.activity_main_options_menu_item_settings:
//                return true;
//        }
//
//
//        return super.onOptionsItemSelected(item);
//    }


    public void onPhotoViewClick(View v, PhotoFile photoFile) {

        TransitionSet transitionSet = new TransitionSet();
        transitionSet.addTransition(TransitionInflater.from(this).inflateTransition(android.R.transition.move));
        transitionSet.setDuration(CARD_TRANSITION_DURATION);

        Bundle bundle = new Bundle();
        bundle.putSerializable(PhotoViewerFragment.PHOTO_FILE_BUNDLE_KEY, photoFile);
        bundle.putString(PhotoViewerFragment.TRANSITION_NAME_BUNDLE_KEY, v.getTransitionName());

        PhotoViewerFragment photoViewerFragment = PhotoViewerFragment.newInstance(bundle);
        photoViewerFragment.setSharedElementEnterTransition(transitionSet);
        photoViewerFragment.setSharedElementReturnTransition(transitionSet);

        Log.d(TAG, v.getTransitionName());

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, photoViewerFragment)
                .addToBackStack(null)
                .addSharedElement(v, v.getTransitionName())
                .commit();
    }

}
