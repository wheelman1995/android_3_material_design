package ru.wheelman.instagram.view.fragments.cards_fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.ObservableLong;
import ru.wheelman.instagram.R;
import ru.wheelman.instagram.room.PhotoFile;
import ru.wheelman.instagram.view.fragments.BaseCardsFragment;

public class CardsFragment extends BaseCardsFragment {
    public static final String MY_FILE_PROVIDER_AUTHORITY = "ru.wheelman.instagram.fileprovider";
    //    public static final int RECYCLER_VIEW_COLUMN_NUMBER = 4;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final String TAG = CardsFragment.class.getSimpleName();
    private File filesDir;
    private String currentPhotoPath;
    private FloatingActionButton fab;
    //    private RecyclerView rv;
//    private CardsFragmentRecyclerViewAdapter adapter;
    //    private ArrayList<PhotoFile> photoFiles;
//    private ActionMode.Callback actionModeCallback;
//    private ActionMode actionMode;
//    private PhotoFile photoFileToDelete;
//    private PhotoFilesViewModel photoFilesViewModel;
    private LiveData<List<PhotoFile>> liveDataPhotoFiles;
    private Observer<List<PhotoFile>> photoFilesObserver;
//    private int positionOfFileBeingDeleted;
//    private View viewBeingDeleted;
//    private Drawable backgroundBackup;
//    private ViewChangedListener viewChangedListener;

    public static CardsFragment newInstance() {
        return new CardsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_cards, container, false);

        initUi(v);

        initListeners();

        updateData();

        return v;
    }

//    private void sendOnViewChangedCallback() {
//        if (viewChangedListener != null) {
//            viewChangedListener.onViewChanged();
//        }
//    }

    @Override
    protected void initListeners() {
        super.initListeners();

        photoFilesObserver = photoFiles -> {
            adapter.setData(photoFiles);
            liveDataPhotoFiles.removeObserver(photoFilesObserver);
        };

        fab.setOnClickListener(v -> {
            dispatchTakePictureIntent();
        });

//            adapter.setOnPhotoViewClickListener((v, photoFile, position) -> {
//                ((MainActivity) getActivity()).onPhotoViewClick(v, photoFile);
//            });
//
//            adapter.setCardViewOnLongClickListener((v, photoFile, position) -> {
//                viewBeingDeleted = v;
//                backgroundBackup = viewBeingDeleted.getBackground();
//                photoFileToDelete = photoFile;
//                positionOfFileBeingDeleted = position;
//                actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(actionModeCallback);
//                viewBeingDeleted.setBackgroundResource(R.drawable.rectangle);
//
//            });
//
//            actionModeCallback = new ActionMode.Callback() {
//                @Override
//                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
//                    mode.getMenuInflater().inflate(R.menu.fragment_cards_action_menu, menu);
//
//                    return true;
//                }
//
//                @Override
//                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
//                    return false;
//                }
//
//                @Override
//                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                    switch (item.getItemId()) {
//                        case R.id.fragment_cards_action_menu_item_delete:
//                            deleteSelectedPhoto();
//                            mode.finish(); // Action picked, so close the CAB
//                            return true;
//                        default:
//                            return false;
//                    }
//                }
//
//                @Override
//                public void onDestroyActionMode(ActionMode mode) {
//                    actionMode = null;
//                    viewBeingDeleted.setBackground(backgroundBackup);
//                }
//            };

        adapter.setHeartViewOnClickListener((v, photoFile, position) -> {
            Log.d(TAG, "setHeartViewOnClickListener position " + position);
            photoFile.setLiked(!photoFile.isLiked());
            ObservableBoolean observableBoolean = photoFilesViewModel.update(photoFile);

            observableBoolean.addObserver((o, newValue) -> {
                boolean success = (boolean) newValue;

                if (success) {
                    getActivity().runOnUiThread(() -> {
                        adapter.changeItem(photoFile, position, photoFile.isLiked());
                        sendOnViewChangedCallback();
                    });
                }
                observableBoolean.deleteObservers();

            });

        });
    }

    public void updateData() {
        if (liveDataPhotoFiles.hasObservers()) {
            liveDataPhotoFiles.removeObservers(this);
        }
        liveDataPhotoFiles.observe(this, photoFilesObserver);
    }


//    private void deleteSelectedPhoto() {
//
//        Timer timer = new Timer();
//
//        TimerTask timerTask = new TimerTask() {
//
//            PhotoFile photoFileToDelete = CardsFragment.this.photoFileToDelete;
//            int positionOfFileBeingDeleted = CardsFragment.this.positionOfFileBeingDeleted;
//
//            @Override
//            public void run() {
//
//                ObservableBoolean observableBoolean = photoFilesViewModel.delete(photoFileToDelete);
//                observableBoolean.addObserver((o, newValue) -> {
//                    boolean success = (boolean) newValue;
//
//                    if (success) {
//                        new File(photoFileToDelete.getPath()).delete();
//
//                        getActivity().runOnUiThread(() -> {
//                            adapter.removeItem(positionOfFileBeingDeleted);
//                            sendOnViewChangedCallback();
//                        });
//
//                        observableBoolean.deleteObservers();
//                    }
//                });
//            }
//        };
//
//        Snackbar.make(rv, R.string.cards_fragment_snackbar_photo_deletion_warning, Snackbar.LENGTH_LONG)
//                .setAction(R.string.cards_fragment_snackbar_photo_deletion_action_undo, v -> {
//                    timer.cancel();
//                }).show();
//
//        timer.schedule(timerTask, 3000);
//    }


    @Override
    protected void initUi(View v) {
//        photoFilesViewModel = ViewModelProviders.of(this).get(PhotoFilesViewModel.class);
        super.initUi(v);
        liveDataPhotoFiles = photoFilesViewModel.getAllPhotoFiles();

        fab = v.findViewById(R.id.fab_fragment_cards);
//        rv = v.findViewById(R.id.rv_fragment_cards);
//        rv.setHasFixedSize(true);
//        rv.setLayoutManager(new GridLayoutManager(v.getContext(), RECYCLER_VIEW_COLUMN_NUMBER));

        filesDir = v.getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

//        photoFiles = new ArrayList<>();
//        photoFiles.addAll(Arrays.asList(filesDir.listFiles(pathname -> pathname.getName().endsWith(".jpg"))));
//        Collections.sort(photoFiles, (o1, o2) -> o1.lastModified() > o2.lastModified() ? -1 : 1);


//        adapter = new CardsFragmentRecyclerViewAdapter(Collections.emptyList());
//        rv.setAdapter(adapter);
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            File photoFile = null;

            try {
                photoFile = createImageFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getContext(), MY_FILE_PROVIDER_AUTHORITY, photoFile);
                Log.d(TAG, "photoURI " + photoURI.toString());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
//            Bundle extras = data.getExtras();
//            Bitmap imageBitmap = (Bitmap) extras.get("data");

            File newPhoto = new File(currentPhotoPath);
            PhotoFile photoFile = new PhotoFile(newPhoto.getAbsolutePath(), false, newPhoto.lastModified());
            ObservableLong observableLong = photoFilesViewModel.insert(photoFile);

            observableLong.addObserver((o, newValue) -> {
                Long rowId = (Long) newValue;
                if (rowId != null) {
                    photoFile.setId(rowId);
                    getActivity().runOnUiThread(() -> {
                        adapter.insertItem(photoFile);
                        rv.smoothScrollToPosition(0);
                    });
                }
                observableLong.deleteObservers();
            });
        }
    }

    private File createImageFile() throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        sdf.setTimeZone(TimeZone.getDefault());
        String timeStamp = sdf.format(new Date());
        Log.d(TAG, "timestamp " + timeStamp);
        String imageFileName = "JPEG_" + timeStamp + "_";

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                filesDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.d(TAG, "currentPhotoPath " + currentPhotoPath);
        return image;
    }
}
