package ru.wheelman.instagram;

<<<<<<< HEAD
import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import ru.wheelman.instagram.view.App;

=======
import android.content.Context;
import android.content.SharedPreferences;

>>>>>>> 5e4aa51f2e4c7aab6ca64935e6a0d665693a7080
import static android.content.Context.MODE_PRIVATE;
import static ru.wheelman.instagram.Constants.MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY;

public class Prefs {
<<<<<<< HEAD
    private static volatile Prefs instance;
    private SharedPreferences sp;

    private Prefs() {
        sp = App.getInstance().getSharedPreferences(Constants.MAIN_SHARED_PREFERENCES_NAME, MODE_PRIVATE);
    }

    public static Prefs getInstance() {

        Prefs localInstance = instance;

        if (localInstance == null) {
            synchronized (Prefs.class) {
                localInstance = instance;
                if (localInstance == null) {
                    localInstance = instance = new Prefs();
=======
    private static volatile Prefs INSTANCE;
    private SharedPreferences sp;

    private Prefs() {
        sp = App.getApp().getSharedPreferences(Constants.MAIN_SHARED_PREFERENCES_NAME, MODE_PRIVATE);
    }

    public static Prefs getINSTANCE() {

        Prefs localInstance = INSTANCE;

        if (localInstance == null) {
            synchronized (Prefs.class) {
                localInstance = INSTANCE;
                if (localInstance == null) {
                    localInstance = INSTANCE = new Prefs();
>>>>>>> 5e4aa51f2e4c7aab6ca64935e6a0d665693a7080
                }
            }
        }
        return localInstance;
    }

    public boolean isBlackThemeEnabled() {
        return sp.getBoolean(MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY, false);
    }

<<<<<<< HEAD
    @SuppressLint("ApplySharedPref")
    public void setBlackThemeEnabled(boolean blackThemeEnabled) {
//    needs to be executed in the main thread!
        sp.edit().putBoolean(MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY, blackThemeEnabled).commit();
=======
    public void setBlackThemeEnabled(boolean blackThemeEnabled) {
        sp.edit().putBoolean(MAIN_SHARED_PREFERENCES_BLACK_THEME_ENABLED_KEY, blackThemeEnabled).apply();
>>>>>>> 5e4aa51f2e4c7aab6ca64935e6a0d665693a7080
    }
}
