package ru.wheelman.instagram;

import android.app.Application;

public class App extends Application {

    private static Application app;

    public static Application getApp() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;
    }
}
