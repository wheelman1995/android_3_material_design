package ru.wheelman.instagram.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = PhotoFile.class, version = 1)
public abstract class PhotoFilesDatabase extends RoomDatabase {
    public abstract PhotoFilesDAO photoFilesDAO();

    public static final String DATABASE_NAME = "photo_files_database";
    private static volatile PhotoFilesDatabase INSTANCE;

    public static PhotoFilesDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PhotoFilesDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PhotoFilesDatabase.class, DATABASE_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
