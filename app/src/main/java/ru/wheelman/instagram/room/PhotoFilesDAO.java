package ru.wheelman.instagram.room;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface PhotoFilesDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(PhotoFile photoFile);

    @Query("select * from PhotoFile order by lastModified desc")
    LiveData<List<PhotoFile>> getAllPhotoFiles();

    @Delete
    int delete(PhotoFile photoFile);

    @Update()
    int update(PhotoFile photoFile);

    @Query("select * from PhotoFile where isLiked = 1 order by lastModified desc")
    List<PhotoFile> getAllLikedPhotoFiles();
}
