package ru.wheelman.instagram.room;

import android.provider.BaseColumns;

import java.io.Serializable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class PhotoFile implements Serializable {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = BaseColumns._ID)
<<<<<<< HEAD
    private long id;
=======
    private int id;
>>>>>>> 5e4aa51f2e4c7aab6ca64935e6a0d665693a7080

    private String path;
    @ColumnInfo(typeAffinity = ColumnInfo.INTEGER)
    private boolean isLiked;
    private long lastModified;

    public PhotoFile(String path, boolean isLiked, long lastModified) {
        this.path = path;
        this.isLiked = isLiked;
        this.lastModified = lastModified;
    }

<<<<<<< HEAD
    public long getId() {
        return id;
    }

    public void setId(long id) {
=======
    public int getId() {
        return id;
    }

    public void setId(int id) {
>>>>>>> 5e4aa51f2e4c7aab6ca64935e6a0d665693a7080
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public long getLastModified() {
        return lastModified;
    }

    public void setLastModified(long lastModified) {
        this.lastModified = lastModified;
    }
}
