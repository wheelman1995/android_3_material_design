package ru.wheelman.instagram;

import java.util.List;
import java.util.Observable;

import ru.wheelman.instagram.room.PhotoFile;

public class ObservablePhotoFilesList extends Observable {

    private List<PhotoFile> list;

    public void set(List<PhotoFile> newValue) {
        this.list = newValue;
        setChanged();
        notifyObservers(list);
    }
}
