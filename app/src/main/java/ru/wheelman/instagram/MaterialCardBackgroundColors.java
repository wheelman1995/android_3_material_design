package ru.wheelman.instagram;

import java.util.ArrayList;
import java.util.List;

public final class MaterialCardBackgroundColors {

    public static final List<Integer> COLORS = new ArrayList<Integer>() {{
        add(R.color.red);
        add(R.color.orange);
        add(R.color.yellow);
        add(R.color.green);
        add(R.color.light_blue);
        add(R.color.blue);
        add(R.color.purple);
    }};


    public static final int COLORS_SIZE = COLORS.size();
}
