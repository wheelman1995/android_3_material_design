package ru.wheelman.instagram;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.room.PhotoFile;
import ru.wheelman.instagram.room.PhotoFilesDAO;
import ru.wheelman.instagram.room.PhotoFilesDatabase;

public class PhotoFilesRepositoryImpl implements PhotoFilesRepository {

    private static PhotoFilesRepositoryImpl photoFilesRepositoryImpl;
    private PhotoFilesDAO photoFilesDAO;
    private LiveData<List<PhotoFile>> allPhotoFiles;
    private PhotoFilesRepositoryImpl(Application application) {
        PhotoFilesDatabase photoFilesDatabase = PhotoFilesDatabase.getDatabase(application);
        photoFilesDAO = photoFilesDatabase.photoFilesDAO();
        allPhotoFiles = photoFilesDAO.getAllPhotoFiles();

    }

    public static synchronized PhotoFilesRepository getRepository(Application application) {
        if (photoFilesRepositoryImpl == null) {
            photoFilesRepositoryImpl = new PhotoFilesRepositoryImpl(application);
        }
        return (PhotoFilesRepository) photoFilesRepositoryImpl;
    }

    @Override
    public LiveData<List<PhotoFile>> getAllPhotoFiles() {
        return allPhotoFiles;
    }

    @Override
    public ObservableBoolean delete(PhotoFile photoFile) {
        ObservableBoolean observableBoolean = new ObservableBoolean();
        new DeleteAsyncTask(photoFilesDAO).execute(photoFile, observableBoolean);
        return observableBoolean;
    }

    @Override
    public ObservableBoolean update(PhotoFile photoFile) {
        ObservableBoolean observableBoolean = new ObservableBoolean();
        new UpdateAsyncTask(photoFilesDAO).execute(photoFile, observableBoolean);
        return observableBoolean;
    }


    @Override
    public ObservableBoolean insert(PhotoFile photoFile) {
        ObservableBoolean observableBoolean = new ObservableBoolean();
        new InsertAsyncTask(photoFilesDAO).execute(photoFile, observableBoolean);
        return observableBoolean;
    }

    private static class InsertAsyncTask extends AsyncTask<Object, Void, Long> {

        private PhotoFilesDAO mAsyncTaskDao;

        InsertAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final Object... params) {
            ObservableBoolean observableBoolean = (ObservableBoolean) params[1];
            // -1 means that an error occurred
            if (mAsyncTaskDao.insert((PhotoFile) params[0]) != -1) {
                observableBoolean.set(true);
            }

            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Object, Void, Integer> {

        private PhotoFilesDAO mAsyncTaskDao;

        DeleteAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... params) {
            ObservableBoolean observableBoolean = (ObservableBoolean) params[1];
            if (mAsyncTaskDao.delete((PhotoFile) params[0]) == 1) {
                observableBoolean.set(true);
            }

            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Object, Void, Integer> {

        private PhotoFilesDAO mAsyncTaskDao;

        UpdateAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... params) {
            ObservableBoolean observableBoolean = (ObservableBoolean) params[1];
            if (mAsyncTaskDao.update((PhotoFile) params[0]) == 1) {
                observableBoolean.set(true);
            }

            return null;
        }
    }
}
