package ru.wheelman.instagram.repository;

import java.util.List;

import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.ObservableLong;
import ru.wheelman.instagram.ObservablePhotoFilesList;
import ru.wheelman.instagram.room.PhotoFile;

public interface IPhotoFilesRepository {
    ObservableLong insert(PhotoFile photoFile);
    LiveData<List<PhotoFile>> getAllPhotoFiles();

    ObservableBoolean delete(PhotoFile photoFile);

    ObservableBoolean update(PhotoFile photoFile);

    ObservablePhotoFilesList getAllLikedPhotoFiles();
}
