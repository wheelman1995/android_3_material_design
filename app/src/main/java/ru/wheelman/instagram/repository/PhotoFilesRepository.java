package ru.wheelman.instagram.repository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import java.util.List;

import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.ObservableBoolean;
import ru.wheelman.instagram.ObservableLong;
import ru.wheelman.instagram.ObservablePhotoFilesList;
import ru.wheelman.instagram.room.PhotoFile;
import ru.wheelman.instagram.room.PhotoFilesDAO;
import ru.wheelman.instagram.room.PhotoFilesDatabase;

public class PhotoFilesRepository implements IPhotoFilesRepository {

    private static PhotoFilesRepository photoFilesRepository;
    private PhotoFilesDAO photoFilesDAO;
    private LiveData<List<PhotoFile>> allPhotoFiles;

    private PhotoFilesRepository(Application application) {
        PhotoFilesDatabase photoFilesDatabase = PhotoFilesDatabase.getDatabase(application);
        photoFilesDAO = photoFilesDatabase.photoFilesDAO();
        allPhotoFiles = photoFilesDAO.getAllPhotoFiles();

    }

    public static synchronized IPhotoFilesRepository getRepository(Application application) {
        if (photoFilesRepository == null) {
            photoFilesRepository = new PhotoFilesRepository(application);
        }
        return (IPhotoFilesRepository) photoFilesRepository;
    }

    @Override
    public LiveData<List<PhotoFile>> getAllPhotoFiles() {
        return allPhotoFiles;
    }

    @Override
    public ObservableBoolean delete(PhotoFile photoFile) {
        ObservableBoolean observableBoolean = new ObservableBoolean();
        new DeleteAsyncTask(photoFilesDAO).execute(photoFile, observableBoolean);
        return observableBoolean;
    }

    @Override
    public ObservableBoolean update(PhotoFile photoFile) {
        ObservableBoolean observableBoolean = new ObservableBoolean();
        new UpdateAsyncTask(photoFilesDAO).execute(photoFile, observableBoolean);
        return observableBoolean;
    }

    @Override
    public ObservablePhotoFilesList getAllLikedPhotoFiles() {
        ObservablePhotoFilesList observablePhotoFilesList = new ObservablePhotoFilesList();

        new Thread(() -> {
            observablePhotoFilesList.set(photoFilesDAO.getAllLikedPhotoFiles());
        }).start();

        return observablePhotoFilesList;
    }


    @Override
    public ObservableLong insert(PhotoFile photoFile) {
        ObservableLong observableLong = new ObservableLong();
        new InsertAsyncTask(photoFilesDAO).execute(photoFile, observableLong);
        return observableLong;
    }

    private static class InsertAsyncTask extends AsyncTask<Object, Void, Long> {

        private PhotoFilesDAO mAsyncTaskDao;

        InsertAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Long doInBackground(final Object... params) {
            ObservableLong observableLong = (ObservableLong) params[1];
            long insertResult = mAsyncTaskDao.insert((PhotoFile) params[0]);
            // -1 means that an error occurred

            observableLong.set(insertResult == -1 ? null : insertResult);

            return null;
        }
    }

    private static class DeleteAsyncTask extends AsyncTask<Object, Void, Integer> {

        private PhotoFilesDAO mAsyncTaskDao;

        DeleteAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... params) {
            ObservableBoolean observableBoolean = (ObservableBoolean) params[1];
            if (mAsyncTaskDao.delete((PhotoFile) params[0]) == 1) {
                observableBoolean.set(true);
            }

            return null;
        }
    }

    private static class UpdateAsyncTask extends AsyncTask<Object, Void, Integer> {

        private static final String TAG = "UpdateAsyncTask";
        private PhotoFilesDAO mAsyncTaskDao;

        UpdateAsyncTask(PhotoFilesDAO dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Integer doInBackground(final Object... params) {
            ObservableBoolean observableBoolean = (ObservableBoolean) params[1];
            int update = mAsyncTaskDao.update((PhotoFile) params[0]);
            Log.d(TAG, String.valueOf(update));
            if (update == 1) {
                observableBoolean.set(true);
            }

            return null;
        }
    }
}
