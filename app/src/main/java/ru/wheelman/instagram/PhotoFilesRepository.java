package ru.wheelman.instagram;

import java.util.List;

import androidx.lifecycle.LiveData;
import ru.wheelman.instagram.room.PhotoFile;

public interface PhotoFilesRepository {
    ObservableBoolean insert(PhotoFile photoFile);
    LiveData<List<PhotoFile>> getAllPhotoFiles();

    ObservableBoolean delete(PhotoFile photoFile);

    ObservableBoolean update(PhotoFile photoFile);
}
