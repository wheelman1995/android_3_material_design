package ru.wheelman.instagram;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;

public class CardsFragmentFloatingActionButtonBehavior extends FloatingActionButton.Behavior {

    private static final String TAG = "CardsFragmentFAB";
    private LinearInterpolator linearInterpolator;

    public CardsFragmentFloatingActionButtonBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
        linearInterpolator = new LinearInterpolator();
        Log.d(TAG, "CardsFragmentFloatingActionButtonBehavior");
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout,
                               @NonNull FloatingActionButton child,
                               @NonNull View target,
                               int dxConsumed,
                               int dyConsumed,
                               int dxUnconsumed,
                               int dyUnconsumed,
                               int type) {

        Log.d(TAG, "onNestedScroll");

        if (dyConsumed > 0) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            int fabEndMargin = layoutParams.getMarginEnd();
            child.animate()
                    .translationX(child.getWidth() + fabEndMargin)
                    .setInterpolator(linearInterpolator)
                    .setDuration(100)
                    .start();
        } else if (dyConsumed < 0) {
            child.animate()
                    .translationX(0)
                    .setInterpolator(linearInterpolator)
                    .setDuration(100)
                    .start();
        }


    }

    @Override
    public boolean onStartNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View directTargetChild, @NonNull View target, int axes, int type) {
        Log.d(TAG, "onStartNestedScroll");
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }
}
